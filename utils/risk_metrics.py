from typing import Dict, Union

import numpy as np
import pandas as pd
from pydantic import BaseModel
from riskfolio import RiskFunctions


class ModelConfig(BaseModel):
    class Config:
        arbitrary_types_allowed = True


class risk_metrics_selector_config(ModelConfig):
    wgt_tangency_constrained: Union[pd.DataFrame, np.ndarray]
    wgt_efficient_frontier_constrained: Union[pd.DataFrame, np.ndarray]
    wgt_risk_parity: Union[pd.DataFrame, np.ndarray]
    wgt_tangency_unconstrained: Union[pd.DataFrame, np.ndarray]
    wgt_efficient_frontier_unconstrained: Union[pd.DataFrame, np.ndarray]
    cov_matrix: Union[pd.DataFrame, np.ndarray]


class risk_metrics:

    r"""
    Class that creates object with all properties needed to calculate risk metrics &
    risk contributions across risk/return optimized portfolios

    Parameters
    ----------
    """

    def __init__(
        self
    ):
        self


    def risk_measurement(
        self,
        port_ret: np.ndarray,
        alpha: float,
    ) -> Dict:
        # initialize the risk metrics class object
        # estimate historical sim returns of port assuming opt weights through time

        rets = port_ret
        VaR_Hist = RiskFunctions.VaR_Hist(rets, alpha=alpha)
        CVaR_Hist = RiskFunctions.CVaR_Hist(rets, alpha=alpha)
        wr = RiskFunctions.WR(rets)
        mdd = RiskFunctions.MDD_Abs(rets)
        add = RiskFunctions.ADD_Abs(rets)

        return {
            "returns": rets,
            "VaR": VaR_Hist,
            "CVaR": CVaR_Hist,
            "wr": wr,
            "mdd": mdd,
            "add": add,
        }
