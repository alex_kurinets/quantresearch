import logging
import warnings
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
import pandas_market_calendars as mcal
from pandas._libs.tslibs.parsing import guess_datetime_format

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

from argparse import ArgumentParser

from utils.risk_metrics import risk_metrics

warnings.filterwarnings("ignore")
pd.options.display.float_format = "{:.4%}".format
import pandas as pd

nyse = mcal.get_calendar("NYSE")

now = datetime.utcnow()
timestamp = now.strftime("%Y%m%d%H%M%S")


def format_date(
    daily_rets: pd.DataFrame, start_year=None, end_year=None
) -> pd.DataFrame:
    if start_year != None:
        start_dt = datetime.strptime(daily_rets["DATE"].iloc[0], "%d-%b")
        start_dt = pd.to_datetime(f"{start_year}-{start_dt.month}-{start_dt.day}")
        end_dt = datetime.strptime(daily_rets["DATE"].iloc[-1], "%d-%b")
        end_dt = pd.to_datetime(f"{end_year}-{end_dt.month}-{end_dt.day}")
        datetime_index = nyse.valid_days(start_date=start_dt, end_date=end_dt)
        daily_rets["DATE"] = datetime_index
    else:
        daily_rets["DATE"] = pd.to_datetime(daily_rets["DATE"]) + pd.offsets.MonthEnd(0)

    return daily_rets


if __name__ == "__main__":
    parser = ArgumentParser(description="")

    parser.add_argument(
        "--alpha",
        type=float,
        help="level of significance -- tail risk estimates",
        default=1e-2,
    )

    args = parser.parse_args()

    # unpack arguments
    alpha = args.alpha
    annualization_factor = args.annualization_factor
    init_capital_balance = args.init_capital_balance
    start_year = args.start_year
    end_year = args.end_year

    # open workbook with daily data needed to estimate daily returns
    monthly_rets = pd.read_csv("data/monthly_rets.csv")
    LOGGER.info("imply monthly returns -- account by account")
    monthly_rets = monthly_rets.loc[:, ~monthly_rets.columns.str.contains("^Unnamed")]
    daily_rets.dropna(subset=["FINAL NET"], inplace=True)
    daily_rets.reset_index(inplace=True, drop=True)

    format = guess_datetime_format(daily_rets["DATE"].iloc[0])
    if format == "%Y-%m-%d":
        start_year = None
    daily_rets = format_date(daily_rets, start_year, end_year)

    # imply daily returns
    daily_ret_list = []
    capital_list = []
    for ix in range(0, len(daily_rets)):
        daily_pnl = daily_rets["FINAL NET"].iloc[ix]
        if ix == 0:
            capital_balance = init_capital_balance

        else:
            capital_balance = capital_balance + daily_pnl
        daily_ret = (capital_balance + daily_pnl) / capital_balance - 1
        capital_list.append(capital_balance)
        daily_ret_list.append(daily_ret)
    daily_rets["capital_balance"] = capital_list
    daily_rets["ret"] = daily_ret_list

    risk_metrics_detail = risk_metrics()

    # 1.a calculate portfolio level riskmetrics
    risk_summary_results_list = []
    rm_dict = risk_metrics_detail.risk_measurement(
        daily_rets["ret"],
        args.alpha,
    )
    ann_return = np.mean(rm_dict["returns"]) * annualization_factor
    volatility = np.std(rm_dict["returns"]) * (annualization_factor**0.5)
    sharpe = ann_return / volatility
    risk_summary_results_list.append(
        {
            "port_risk_metrics": {
                "a) ann_return": f"{ann_return:.2%}",
                "b) ann_volatility": f"{volatility:.2%}",
                "c) sharpe": f"{sharpe:.2%}",
                f"d) {100 * (1 - alpha)}%_VaR": f"{rm_dict['VaR']:.2%}",
                f"e) {100 * (1 - alpha)}%_CVaR": f"{rm_dict['CVaR']:.2%}",
                "f) worst_realization": f"{rm_dict['wr']:.2%}",
                "g) mean drawdwown": f"{rm_dict['mdd']:.2%}",
                "h) absolute drawdown": f"{rm_dict['add']:.2%}",
            }
        }
    )

    LOGGER.info("put risk summary results to .csv")
    df = pd.concat(
        [pd.DataFrame.from_dict(x, orient="index") for x in risk_summary_results_list]
    ).T
    df.to_csv(f"output/daily_risk_summary_{timestamp}.csv")

    daily_rets.to_csv(f"output/daily_rets_processed_{timestamp}.csv") # output the
    # process daily data